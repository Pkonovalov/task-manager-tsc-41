package ru.pkonovalov.tm.exception.entity;

public class UserNotFoundException extends RuntimeException {

    public UserNotFoundException() {
        super("Error! User not found...");
    }

}
