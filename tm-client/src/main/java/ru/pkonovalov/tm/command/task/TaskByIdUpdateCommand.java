package ru.pkonovalov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.pkonovalov.tm.exception.empty.EmptyNameException;
import ru.pkonovalov.tm.exception.entity.TaskNotFoundException;
import ru.pkonovalov.tm.util.TerminalUtil;

import static ru.pkonovalov.tm.util.ValidationUtil.isEmpty;

public final class TaskByIdUpdateCommand extends AbstractTaskCommand {

    @Nullable
    @Override
    public String commandArg() {
        return null;
    }

    @NotNull
    @Override
    public String commandDescription() {
        return "Update task by id";
    }

    @NotNull
    @Override
    public String commandName() {
        return "task-update-by-id";
    }

    @Override
    public void execute() {
        System.out.println("[UPDATE TASK]");
        System.out.println("[ENTER ID:]");
        @NotNull final String id = TerminalUtil.nextLine();
        if (!endpointLocator.getTaskEndpoint().existsTaskById(endpointLocator.getSession(), id))
            throw new TaskNotFoundException();
        System.out.println("ENTER NAME:");
        @NotNull final String name = TerminalUtil.nextLine();
        if (isEmpty(name)) throw new EmptyNameException();
        System.out.println("ENTER DESCRIPTION:");
        endpointLocator.getTaskEndpoint().updateTaskById(endpointLocator.getSession(), id, name, TerminalUtil.nextLine());

    }

}
