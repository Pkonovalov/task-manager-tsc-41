package ru.pkonovalov.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.pkonovalov.tm.util.TerminalUtil;

public final class ProjectByNameRemoveCommand extends AbstractProjectCommand {

    @Nullable
    @Override
    public String commandArg() {
        return null;
    }

    @NotNull
    @Override
    public String commandDescription() {
        return "Remove project by name";
    }

    @NotNull
    @Override
    public String commandName() {
        return "project-remove-by-name";
    }

    @Override
    public void execute() {
        System.out.println("[REMOVE PROJECT]");
        System.out.println("ENTER NAME:");
        endpointLocator.getProjectEndpoint().removeProjectByName(endpointLocator.getSession(), TerminalUtil.nextLine());
    }

}
