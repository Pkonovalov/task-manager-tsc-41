package ru.pkonovalov.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.pkonovalov.tm.exception.empty.EmptyNameException;
import ru.pkonovalov.tm.util.TerminalUtil;

import static ru.pkonovalov.tm.util.ValidationUtil.isEmpty;

public final class ProjectCreateCommand extends AbstractProjectCommand {

    @Nullable
    @Override
    public String commandArg() {
        return null;
    }

    @NotNull
    @Override
    public String commandDescription() {
        return "Create project";
    }

    @NotNull
    @Override
    public String commandName() {
        return "project-create";
    }

    @Override
    public void execute() {
        System.out.println("[PROJECT CREATE]");
        System.out.println("ENTER NAME:");
        @NotNull final String name = TerminalUtil.nextLine();
        if (isEmpty(name)) throw new EmptyNameException();
        System.out.println("ENTER DESCRIPTION:");
        @NotNull final String description = TerminalUtil.nextLine();
        endpointLocator.getProjectEndpoint().createProject(endpointLocator.getSession(), name, description);
    }

}
