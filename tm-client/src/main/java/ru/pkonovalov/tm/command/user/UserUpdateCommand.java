package ru.pkonovalov.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.pkonovalov.tm.util.TerminalUtil;

public final class UserUpdateCommand extends AbstractUserCommand {

    @Nullable
    @Override
    public String commandArg() {
        return null;
    }

    @NotNull
    @Override
    public String commandDescription() {
        return "Update user profile";
    }

    @NotNull
    @Override
    public String commandName() {
        return "user-update";
    }

    @Override
    public void execute() {
        System.out.println("[UPDATE PROFILE]");
        System.out.println("ENTER FIRST NAME:");
        @NotNull final String firstName = TerminalUtil.nextLine();
        System.out.println("ENTER MIDDLE NAME:");
        @NotNull final String middleName = TerminalUtil.nextLine();
        System.out.println("ENTER LAST NAME:");
        @NotNull final String lastName = TerminalUtil.nextLine();
        endpointLocator.getUserEndpoint().updateUser(endpointLocator.getSession(), firstName, lastName, middleName);
    }

}
