package ru.pkonovalov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.pkonovalov.tm.exception.empty.EmptyNameException;
import ru.pkonovalov.tm.util.TerminalUtil;

import static ru.pkonovalov.tm.util.ValidationUtil.isEmpty;

public final class TaskByNameUpdateCommand extends AbstractTaskCommand {

    @Nullable
    @Override
    public String commandArg() {
        return null;
    }

    @NotNull
    @Override
    public String commandDescription() {
        return "Update task by name";
    }

    @NotNull
    @Override
    public String commandName() {
        return "task-update-by-name";
    }

    @Override
    public void execute() {
        System.out.println("[UPDATE TASK]");
        System.out.println("[ENTER NAME:]");
        @NotNull final String name = TerminalUtil.nextLine();
        if (!endpointLocator.getTaskEndpoint().existsTaskByName(endpointLocator.getSession(), name))
            throw new EmptyNameException();
        System.out.println("ENTER NEW NAME:");
        @NotNull final String nameNew = TerminalUtil.nextLine();
        if (isEmpty(nameNew)) throw new EmptyNameException();
        System.out.println("ENTER DESCRIPTION:");
        endpointLocator.getTaskEndpoint().updateTaskByName(endpointLocator.getSession(), name, nameNew, TerminalUtil.nextLine());
    }

}
