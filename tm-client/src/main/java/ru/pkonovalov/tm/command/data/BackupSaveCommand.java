package ru.pkonovalov.tm.command.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.pkonovalov.tm.command.AbstractCommand;
import ru.pkonovalov.tm.endpoint.Role;


public final class BackupSaveCommand extends AbstractCommand {

    @Nullable
    @Override
    public String commandArg() {
        return null;
    }

    @Nullable
    @Override
    public String commandDescription() {
        return "Save backup to XML";
    }

    @NotNull
    @Override
    public String commandName() {
        return "backup-save";
    }

    @Override
    public @Nullable Role[] commandRoles() {
        return null;
    }

    @Override
    @SneakyThrows
    public void execute() {
        System.out.println("[BACKUP SAVE]");
        endpointLocator.getAdminEndpoint().saveBackup(endpointLocator.getSession());
    }

}
