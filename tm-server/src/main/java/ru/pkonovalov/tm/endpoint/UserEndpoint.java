package ru.pkonovalov.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.pkonovalov.tm.api.endpoint.IUserEndpoint;
import ru.pkonovalov.tm.api.service.ServiceLocator;
import ru.pkonovalov.tm.dto.SessionDTO;
import ru.pkonovalov.tm.dto.UserDTO;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@WebService
public final class UserEndpoint extends AbstractEndpoint implements IUserEndpoint {

    public UserEndpoint() {
        super(null);
    }

    public UserEndpoint(@NotNull final ServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @Override
    @WebMethod
    public boolean existsUserByEmail(
            @WebParam(name = "email", partName = "email") @NotNull final String email
    ) {
        return serviceLocator.getUserService().existsByEmail(email);
    }

    @Override
    @WebMethod
    public boolean existsUserByLogin(
            @WebParam(name = "login", partName = "login") @NotNull final String login
    ) {
        return serviceLocator.getUserService().existsByLogin(login);
    }

    @Override
    @WebMethod
    public void registryUser(
            @WebParam(name = "login", partName = "login") @NotNull final String login,
            @WebParam(name = "password", partName = "password") @NotNull final String password,
            @WebParam(name = "email", partName = "email") @NotNull final String email
    ) {
        serviceLocator.getUserService().create(login, password, email);
    }

    @Override
    @WebMethod
    public void updateUser(
            @WebParam(name = "session", partName = "session") @NotNull final SessionDTO session,
            @WebParam(name = "firstName", partName = "firstName") @NotNull final String firstName,
            @WebParam(name = "lastName", partName = "lastName") @NotNull final String lastName,
            @WebParam(name = "middleName", partName = "middleName") @NotNull final String middleName
    ) {
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getUserService().updateUser(session.getUserId(), firstName, lastName, middleName);
    }

    @Override
    @WebMethod
    public void updateUserPassword(
            @WebParam(name = "session", partName = "session") @NotNull final SessionDTO session,
            @WebParam(name = "password", partName = "password") @NotNull final String password
    ) {
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getUserService().setPassword(session.getUserId(), password);
    }

    @Override
    @Nullable
    @WebMethod
    public UserDTO viewUser(
            @WebParam(name = "session", partName = "session") @NotNull final SessionDTO session
    ) {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getUserService().findById(session.getUserId());
    }

}
