package ru.pkonovalov.tm.enumerated;

import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.pkonovalov.tm.comparator.ComparatorByCreated;
import ru.pkonovalov.tm.comparator.ComparatorByName;
import ru.pkonovalov.tm.comparator.ComparatorByStarted;
import ru.pkonovalov.tm.comparator.ComparatorByStatus;
import ru.pkonovalov.tm.exception.entity.SortNotFoundException;

import java.util.Comparator;

import static ru.pkonovalov.tm.util.ValidationUtil.checkSort;

@Getter
public enum Sort {

    NAME("Sort by name", ComparatorByName.getInstance()),
    CREATED("Sort by created", ComparatorByCreated.getInstance()),
    STARTED("Sort by date start", ComparatorByStarted.getInstance()),
    STATUS("Sort by status", ComparatorByStatus.getInstance());

    @NotNull
    private final Comparator comparator;
    @NotNull
    private final String displayName;

    Sort(@NotNull final String displayName, @NotNull final Comparator comparator) {
        this.displayName = displayName;
        this.comparator = comparator;
    }

    public static @NotNull Sort getSort(@Nullable String s) {
        if (s == null) throw new SortNotFoundException();
        s = s.toUpperCase();
        if (!checkSort(s)) throw new SortNotFoundException();
        return valueOf(s);
    }

    @NotNull
    public Comparator getComparator() {
        return comparator;
    }

}
