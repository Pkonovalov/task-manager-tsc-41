package ru.pkonovalov.tm.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;
import java.io.Serializable;

@Getter
@Setter
@MappedSuperclass
@NoArgsConstructor
public abstract class AbstractEntityDTO extends AbstractOwnerDTO implements Serializable {

    @Column
    @Nullable
    private String userId;

}
