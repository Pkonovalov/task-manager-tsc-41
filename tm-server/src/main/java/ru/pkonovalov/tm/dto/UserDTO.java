package ru.pkonovalov.tm.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.pkonovalov.tm.enumerated.Role;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;

@Getter
@Setter
@Entity(name = "app_user")
@NoArgsConstructor
public class UserDTO extends AbstractOwnerDTO {

    @Column
    @Nullable
    private String email;
    @Column
    @Nullable
    private String firstName;
    @Column
    @Nullable
    private String lastName;
    @Column
    private boolean locked = false;
    @Column
    @Nullable
    private String login;

    @Column
    @Nullable
    private String middleName;

    @Column
    @Nullable
    private String passwordHash;

    @Nullable
    @Enumerated(EnumType.STRING)
    private Role role = Role.USER;

}
