package ru.pkonovalov.tm.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.pkonovalov.tm.dto.SessionDTO;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Getter
@Setter
@Entity
@NoArgsConstructor
@Table(name = "app_session")
@JsonIgnoreProperties(ignoreUnknown = true)
public class Session extends AbstractEntity implements Cloneable {

    @Column
    @Nullable
    private String signature;

    @Column
    @NotNull
    private Long timestamp;

    @Column
    @NotNull
    private String userId;

    @Nullable
    @Override
    public SessionDTO clone() {
        try {
            return (SessionDTO) super.clone();
        } catch (CloneNotSupportedException e) {
            return null;
        }
    }

}
