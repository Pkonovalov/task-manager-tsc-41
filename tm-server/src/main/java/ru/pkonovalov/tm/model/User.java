package ru.pkonovalov.tm.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.pkonovalov.tm.enumerated.Role;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@Entity
@Table(name = "app_user")
@NoArgsConstructor
public class User extends AbstractOwner {

    @Column
    @Nullable
    private String email;
    @Column
    @Nullable
    private String firstName;
    @Column
    @Nullable
    private String lastName;
    @Column
    private boolean locked = false;
    @Column
    @Nullable
    private String login;

    @Column
    @Nullable
    private String middleName;

    @Column
    @Nullable
    private String passwordHash;
    @Nullable
    @OneToMany(mappedBy = "user")
    private List<Project> projects = new ArrayList<>();
    @Nullable
    @Enumerated(EnumType.STRING)
    private Role role = Role.USER;
    @Nullable
    @OneToMany(mappedBy = "user")
    private List<Session> sessions = new ArrayList<>();
    @Nullable
    @OneToMany(mappedBy = "user")
    private List<Task> tasks = new ArrayList<>();

}
