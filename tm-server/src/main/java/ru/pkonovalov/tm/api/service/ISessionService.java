package ru.pkonovalov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.pkonovalov.tm.api.IService;
import ru.pkonovalov.tm.dto.SessionDTO;
import ru.pkonovalov.tm.dto.UserDTO;
import ru.pkonovalov.tm.enumerated.Role;

import java.util.List;

public interface ISessionService extends IService<SessionDTO> {

    boolean checkDataAccess(@Nullable String login, @Nullable String password);

    void close(@NotNull SessionDTO session);

    void closeAll(@NotNull SessionDTO session);

    @Nullable List<SessionDTO> getListSession(@NotNull SessionDTO session);

    @Nullable UserDTO getUser(@NotNull SessionDTO session);

    @NotNull String getUserId(@NotNull SessionDTO session);

    boolean isValid(@NotNull SessionDTO session);

    @Nullable SessionDTO open(@Nullable String login, @Nullable String password);

    @Nullable SessionDTO sign(@Nullable SessionDTO session);


    void validate(@NotNull SessionDTO session, @Nullable Role role);

    void validate(@Nullable SessionDTO session);
}
