package ru.pkonovalov.tm.api.repository;

import org.apache.ibatis.annotations.*;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.pkonovalov.tm.dto.ProjectDTO;
import ru.pkonovalov.tm.model.Project;

import java.util.Comparator;
import java.util.List;

public interface IProjectRepository {

    @Insert("INSERT INTO `app_project` (`name`, `description`, `dateStart`, `dateFinish`, `userId`, `id`, `status`) " +
            "VALUES (#{project.name}, #{project.description}, #{project.dateStart}, #{project.dateFinish}, #{project.userId}, #{project.id}, #{project.status})")
    void add(@Nullable @Param("project") ProjectDTO project);

    @Insert({
            "<script>",
            "INSERT INTO `app_project` ",
            "(`name`, `description`, `dateStart`, `dateFinish`, `userId`, `id`, `status`) ",
            "VALUES" +
                    "<foreach item='e' collection='projectList' open='' separator=',' close=''>" +
                    "(" +
                    "#{e.name},",
            "#{e.description},",
            "#{e.dateStart},",
            "#{e.dateFinish},",
            "#{e.userId},",
            "#{e.id},",
            "#{e.status}" +
                    ")" +
                    "</foreach>",
            "</script>"})
    void addAll(@Nullable @Param("projectList") List<ProjectDTO> projectList);

    @Delete("DELETE FROM `app_project` WHERE `userId` = #{userId}")
    void clear(@NotNull @Param("userId") String userId);

    @Delete("DELETE FROM `app_project`")
    void clearAll();

    @Select("SELECT CASE WHEN COUNT(*) > 0 THEN TRUE ELSE FALSE END " +
            "FROM `app_project` WHERE `userId` = #{userId} AND `id` = #{id} LIMIT 1")
    boolean existsById(@NotNull @Param("userId") String userId, @NotNull @Param("id") String id);

    @Select("SELECT CASE WHEN COUNT(*) > 0 THEN TRUE ELSE FALSE END " +
            "FROM `app_project` WHERE `userId` = #{userId} AND `name` = #{name} LIMIT 1")
    boolean existsByName(@NotNull @Param("userId") String userId, @Nullable @Param("name") String name);

    @NotNull
    @Select("SELECT * FROM `app_project`")
    List<ProjectDTO> findAll();

    @NotNull
    @Select("SELECT * FROM `app_project` WHERE `userId` = #{userId}")
    List<ProjectDTO> findAllOfUser(@NotNull @Param("userId") String userId);

    //TODO check comparator
    @NotNull
    @Select("SELECT * FROM `app_project` WHERE `userId` = #{userId} ORDER BY &{comparator.class}")
    List<ProjectDTO> findAllSort(@NotNull @Param("userId") String userId, @NotNull @Param("comparator") Comparator<ProjectDTO> comparator);

    @Nullable
    @Select("SELECT * FROM `app_project` WHERE `id` = #{id} AND `userId` = #{userId}")
    ProjectDTO findById(@NotNull @Param("userId") String userId, @NotNull @Param("id") String id);

    @Nullable
    @Select("SELECT * FROM `app_project` WHERE `id` = #{id}")
    ProjectDTO findByIdAll(@NotNull @Param("id") String id);

    @Nullable
    @Select("SELECT * FROM `app_project` WHERE `userId` = #{userId} LIMIT 1 OFFSET #{index}")
    ProjectDTO findOneByIndex(@NotNull @Param("userId") String userId, @NotNull @Param("index") Integer index);

    @Nullable
    @Select("SELECT * FROM `app_project` WHERE `userId` = #{userId} AND `name` = #{name} LIMIT 1")
    ProjectDTO findOneByName(@NotNull @Param("userId") String userId, @Nullable @Param("name") String name);

    @Nullable
    @Select("SELECT `id` FROM `app_project` WHERE `userId` = #{userId} LIMIT 1 OFFSET #{index}")
    String getIdByIndex(@NotNull @Param("userId") String userId, @NotNull @Param("index") Integer index);

    @Nullable
    @Select("SELECT `id` FROM `app_project` WHERE `userId` = #{userId} AND `name` = #{name} LIMIT 1")
    String getIdByName(@NotNull @Param("userId") String userId, @NotNull @Param("name") String name);

    @Delete("DELETE FROM `app_project` WHERE `id` = #{project.id}")
    void remove(@Nullable @Param("project") ProjectDTO project);

    @Delete("DELETE FROM `app_project` WHERE `id` = #{id} AND `userId` = #{userId}")
    void removeById(@NotNull @Param("userId") String userId, @Nullable @Param("id") String id);

    @Delete("DELETE FROM `app_project` WHERE `id` IN (SELECT `id` FROM (SELECT `id` FROM `app_project` WHERE `userId` = #{userId} LIMIT #{index}, 1) x)")
    void removeOneByIndex(@NotNull @Param("userId") String userId, @NotNull @Param("index") Integer index);

    @Delete("DELETE FROM `app_project` WHERE `userId` = #{userId} AND `name` = #{name}")
    void removeOneByName(@NotNull @Param("userId") String userId, @NotNull @Param("name") String name);

    @Select("SELECT COUNT(*) FROM `app_project` WHERE `userId` = #{userId}")
    int size(@NotNull @Param("userId") String userId);

    @Select("SELECT COUNT(*) FROM `app_project`")
    int sizeAll();

    @Update("UPDATE `app_project` SET `name` = #{project.name}, `description` = #{project.description}, " +
            "`dateStart` = #{project.dateStart}, `dateFinish` = #{project.dateFinish}, `userId` = #{project.userId}, " +
            "`status` = #{project.status} WHERE `id` = #{project.id}")
    void update(@Nullable @Param("project") ProjectDTO project);
}
