package ru.pkonovalov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.pkonovalov.tm.api.IService;
import ru.pkonovalov.tm.dto.TaskDTO;
import ru.pkonovalov.tm.enumerated.Status;

import java.util.Comparator;
import java.util.List;

public interface ITaskService extends IService<TaskDTO> {

    @Nullable
    TaskDTO add(@NotNull String userId, @Nullable String name, @Nullable String description);

    void addAll(@Nullable List<TaskDTO> list);

    boolean existsByName(@NotNull String userId, @Nullable String name);

    @NotNull
    List<TaskDTO> findAll(@NotNull String userId, @NotNull Comparator<TaskDTO> comparator);

    @Nullable
    TaskDTO findOneByIndex(@NotNull String userId, @NotNull Integer index);

    @Nullable
    TaskDTO findOneByName(@NotNull String userId, @Nullable String name);

    void finishTaskById(@NotNull String userId, @Nullable String id);

    void finishTaskByIndex(@NotNull String userId, @NotNull Integer index);

    void finishTaskByName(@NotNull String userId, @Nullable String name);

    @Nullable
    String getIdByIndex(@NotNull String userId, @NotNull Integer index);

    void removeOneById(@NotNull String userId, @Nullable String id);

    void removeOneByIndex(@NotNull String userId, @NotNull Integer index);

    void removeOneByName(@NotNull String userId, @Nullable String name);

    void setTaskStatusById(@NotNull String userId, @Nullable String id, @NotNull Status status);

    void setTaskStatusByIndex(@NotNull String userId, @NotNull Integer index, @NotNull Status status);

    void setTaskStatusByName(@NotNull String userId, @Nullable String name, @NotNull Status status);

    void startTaskById(@NotNull String userId, @Nullable String id);

    void startTaskByIndex(@NotNull String userId, @NotNull Integer index);

    void startTaskByName(@NotNull String userId, @Nullable String name);

    void updateTaskById(@NotNull String userId, @Nullable String id, @Nullable String name, @Nullable String description);

    void updateTaskByIndex(@NotNull String userId, @NotNull Integer index, @Nullable String name, @Nullable String description);

    void updateTaskByName(@NotNull String userId, @Nullable String name, @Nullable String nameNew, @Nullable String description);

}
