package ru.pkonovalov.tm.api;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.pkonovalov.tm.dto.AbstractEntityDTO;

import java.util.List;

public interface IRepository<E extends AbstractEntityDTO> {

    @NotNull E add(@Nullable E entity);

    void clear(@NotNull String userId);

    void clear();

    boolean existsById(@NotNull String userId, @NotNull String id);

    @NotNull
    List<E> findAll(@NotNull String userId);

    @Nullable
    List<E> findAll();

    @Nullable
    E findById(@NotNull String id);

    @Nullable
    E findById(@NotNull String userId, @NotNull String id);

    void remove(@Nullable E entity);

    void removeById(@NotNull String userId, @Nullable String id);

    int size();

    int size(@NotNull String userId);

    void update(@Nullable E entity);

}
