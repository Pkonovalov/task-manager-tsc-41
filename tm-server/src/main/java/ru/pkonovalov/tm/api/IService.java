package ru.pkonovalov.tm.api;

import ru.pkonovalov.tm.dto.AbstractEntityDTO;

public interface IService<E extends AbstractEntityDTO> extends IRepository<E> {

}
